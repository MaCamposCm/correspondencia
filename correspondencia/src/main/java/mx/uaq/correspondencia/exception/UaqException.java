package mx.uaq.correspondencia.exception;

import lombok.Getter;
import lombok.Setter;
import mx.uaq.correspondencia.enums.msg.MsgError;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Getter
@Setter
public class UaqException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	private MsgError msgError;
	
	public UaqException(Exception e) {
		super(e);
	}
	
	public UaqException(String message) {
		super(message);
	}
	
	public UaqException(MsgError msgError) {
		this.msgError = msgError;
	}
	
	public UaqException(MsgError msgError, Exception e) {
		super(e);
		this.msgError = msgError;
	}
}