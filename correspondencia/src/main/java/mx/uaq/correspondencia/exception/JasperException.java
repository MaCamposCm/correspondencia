package mx.uaq.correspondencia.exception;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 28 mar. 2021
 *
 */

public class JasperException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public JasperException(String message, Exception e) {
		super(message, e);
	}
}