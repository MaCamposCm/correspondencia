package mx.uaq.correspondencia.exception.login;

import org.springframework.security.core.AuthenticationException;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

public class MaxUsersInLoginException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public MaxUsersInLoginException(String msg) {
		super(msg);
	}
}