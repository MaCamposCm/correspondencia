package mx.uaq.correspondencia.enums.msg;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Getter
@AllArgsConstructor
public enum MsgSuccess {

	// ================================== NOTICIAS ==================================
	NOTICIA_CREADA("La noticia ha sido creada correctamente."),
	NOTICIA_ELIMINADA("La noticia ha sido eliminada correctamente."),
	NOTICIA_ACTUALIZADA("La noticia ha sido actualizada correctamente."),
	
	// ================================== NOTIFICACIONES ==================================
	NOTIFICACION_REENVIADA("La notificación ha sido creada correctamente."),
	
	// ================================== CONFIGURACIONES ==================================
	CONFIGURACION_CREADA("La configración ha sido creada correctamente."),
	CONFIGURACION_ACTUALIZADA("La configuracion ha sido creada correctamente."),
	CONFIGURACION_ELIMINADA("La configuración ha sido eliminada correctamente."),
	
	// ================================== USUARIOS ==================================
	USUARIO_CREADO("El usuario ha sido creado correctamente."),
	USUARIO_ACTIVADO("El usuario ha sido activado correctamente."),
	USUARIO_INACTIVADO("El usuario ha sido inactivado correctamente."),
	USUARIO_ACTUALIZADO("El usuario ha sido actualizado correctamente."),
	USUARIO_TELEFONO_ACTUALIZADO("El número de teléfono ha sido actualizado correctamente."),
	USUARIO_CORREO_PERSONAL_ACTUALIZADO("El correo personal ha sido actualizado correctamente."),
	
	// ================================== PERIODOS ==================================
	PERIODO_AGREGADO("El periodo ha sido agregado correctamente."),
	PERIODO_ELIMINADO("El periodo ha sido eliminado correctamente."),
	PERIODO_ACTUALIZADO("El periodo ha sido actualizado correctamente."),
	
	// ================================== PREGUNTAS ==================================
	PREGUNTA_CREADA("La pregunta ha sido creada correctamente."),
	PREGUNTA_ELIMINADA("La pregunta ha sido eliminada correctamente."),
	PREGUNTA_ACTUALIZADA("La pregunta ha sido actualizada correctamente."),
	
	// ================================== PREGUNTA AYUDAS ==================================
	PREGUNTA_AYUDA_REGISTRADA("La ayuda ha sido creada correctamente."),
	PREGUNTA_AYUDA_ACTUALIZADA("La ayuda ha sido actualizada correctamente."),
	
	// ================================== FOTOGRAFIAS ==================================
	FOTOGRAFIA_EVALUADA("La evaluación ha sido relizada correctamente."),
	FOTOGRAFIA_GUARDADA("La fotografía ha sido guardada correctamente y ha sido enviada para su revisión."),
	
	// ================================== SOLICITUDES ==================================
	SOLICITUD_REGISTRADA("La solicitud ha sido registrada correctamente."),
	SOLICITUD_VALIDACION_TERMINADA("La validación ha sido terminada correctamente."),
	SOLICITUD_CAMBIO_ESTATUS("El estatus de la solicitud ha sido cambiado correctamente."),
	
	// ================================== RESPUESTAS ==================================
	RESPUESTA_ACEPTADA("La respuesta ha sido aceptada correctamente."),
	RESPUESTA_GUARDADA("La respuesta ha sido guardada correctamente."),
	RESPUESTA_ELIMINADA("La respuesta ha sido eliminada correctamente."),
	
	// ================================== TAREAS PROGRAMADAS ==================================
	TAREA_PROGRAMADA_DETENIDA("La tarea programada ha sido detenida correctamente."),
	TAREA_PROGRAMADA_INICIADA("La tarea programada ha sido iniciada correctamente."),
	TAREA_PROGRAMADA_REINICIADA("La tarea programada ha sido reiniciada correctamente.");
	
	private String message;
	public static String ESTATUS = "success";
}