package mx.uaq.correspondencia.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Getter
@AllArgsConstructor
public enum EnumEstatusUsuario {
	
	ACTIVO("ACTIVO"),
	INACTIVO("INACTIVO"),
	BLOQUEADO("BLOQUEADO");
	
	private String estatus;
}