package mx.uaq.correspondencia.enums.msg;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Getter
@AllArgsConstructor
public enum MsgNotification {

	// ================================== USUARIOS ==================================
	USUARIO_ACTIVADO("Este usuario ya se encuentra activo."),
	USUARIO_INACTIVADO("Este usuario ya se encuentra inactivo."),
	
	// ================================== FOTOGRAFIAS ==================================
	FOTOGRAFIA_ESTATUS_ACTUAL("La fotografía ya cuenta con ese estatus."),
	
	// ================================== SOLICITUDES ==================================
	SOLICITUD_PROCESADA("Esta solicitud ya ha sido validada."),
	SOLICITUD_ESTATUS_IDENTICO("Esta solicitud ya cuenta con ese estatus."),
	
	// ================================== TAREAS PROGRAMADAS ==================================
	TAREA_PROGRAMADA_YA_DETENIDA("La tarea programada ya se encuentra detenida."),
	TAREA_PROGRAMADA_YA_INICIADA("La tarea programada ya se encuentra iniciada.");
	
	private String message;
	public static String ESTATUS = "notification";
}