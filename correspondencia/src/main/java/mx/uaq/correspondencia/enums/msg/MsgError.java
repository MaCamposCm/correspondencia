package mx.uaq.correspondencia.enums.msg;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Getter
@AllArgsConstructor
public enum MsgError {
	
	// ================================== GENERAL ==================================
	NO_IDENTIFICADO("Error general del sistema."),
	
	// ================================== USUARIOS ==================================
	USUARIO_BLOQUEADO("Error al bloquear el usuario."),
	USUARIO_ENCONTRADO("Ocurrió un error al buscar el usuario."),
	USUARIO_INACTIVADO("Ocurrió un error al inactivar al usuario."),
	USUARIO_REGISTRADO("Ocurrió un error al registrar el usuario."),
	USUARIO_DESBLOQUEADO("Ocurrió un error al desbloquear al usuario."),
	USUARIO_NUMERO_INTENTOS("Error al reiniciar el número de intentos fallidos."),
	USUARIO_CORREO_ACTUALIZADO("Ocurrió un error al actualizar el correo personal."),
	USUARIO_TELEFONO_ACTUALIZADO("Ocurrió un error al actualizar el número de teléfono."),
	
	// ================================== NOTICIAS ==================================
	NOTICIA_ELIMINADA("Ocurrió un error al eliminar la noticia."),
	NOTICIA_REGISTRADA("Ocurrió un error al registrar la noticia."),
	
	// ================================== NOTIFICACIONES ==================================
	NOTIFICACION_REENVIADA("Ocurrió un error al reenviar la notificación."),
	
	// ================================== CONFIGURACIONES ==================================
	CONFIGURACION_ELIMINADA("Ocurrió un error al eliminar la configuración."),
	CONFIGURACION_REGISTRADA("Ocurrió un error al registrar la configuración."),
	CONFIGURACION_CLAVE_USADA("La clave que intenta registrar ya está en uso."),
	
	// ================================== PERIODOS ==================================
	PERIODO_AGREGADO("Ocurrió un error al agregar el periodo."),
	PERIODO_ELIMINADO("Ocurrió un error al eliminar el periodo."),
	PERIODO_INVALIDO("No hay ningún periodo de registro habilitado."),
	PERIODO_FECHAS_INCORRECTAS("La fecha de inicio no puede ser mayor a la final."),
	PERIODO_NIVEL_USADO("El nivel que intenta agregar ya esta en uso en las mismas fechas."),
	
	// ================================== PREGUNTAS ==================================
	PREGUNTAS_CARGADAS("Ocurrió un error al cargar las preguntas."),
	PREGUNTA_ELIMINADA("Ocurrió un error al eliminar la pregunta."),
	PREGUNTA_REGISTRADA("Ocurrió un error al registrar la pregunta."),
	PREGUNTA_USADA("No puedes actualizar o borrar una pregunta que esta en uso."),
	PREGUNTA_CONTESTADA("No puedes validar esta pregunta ya que no ha sido contestada por el aspirante."),
	
	// ================================== PREGUNTA AYUDAS ==================================
	AYUDA_CARGADA("Ocurrió un error al cargar la ayuda."),
	PREGUNTA_AYUDA_REGISTRADA("Ocurrió un error al registrar la ayuda para la pregunta."),
	
	// ================================== FOTOGRAFIAS ==================================
	FOTOGRAFIA_EVALUADA("Ocurrió un error al evaluar la fotografía."),
	FOTOGRAFIA_GUARDADA("Ocurrió un error al guardar la fotografía."),
	FOTOGRAFIA_OBTENIDA("Ocurrió un error al obtener la fotografía."),
	FOTOGRAFIA_RECHAZADA("No se puede rechazar una fotografía sin las observaciones."),
	FOTOGRAFIA_SOLICITUD_PROCESADA("No puedes actualizar la fotografía que este en proceso de validación o aceptada."),
	
	// ================================== SOLICITUDES ==================================
	SOLICITUD_TERMINADA("Ocurrió un error al finalizar el registro."),
	SOLICITUD_REGISTRADA("Ocurrió un error al registrar la solicitud."),
	SOLICITUD_FALTANTES("Faltan algunas preguntas requeridas por responder."),
	SOLICITUD_VALIDACIONES_FALTANTE("Faltan algunos documentos por validar."),
	SOLICITUD_VALIDACION_TERMINADA("Ocurrió un error al terminar la validación."),
	SOLICITUD_ESTATUS_INVALIDO("Esta solicitud aún no está lista para su validación."),
	SOLICITUD_CAMBIO_ESTATUS("Ocurrió un error al cambiar el estatus de la solicitud."),
	SOLICITUD_PROCESADA("No puede editar una solicitud que ha sido enviada para su revisión."),
	SOLICITUD_FOTOGRAFIA_NO_VALIDADA("No puedes validar la solicitud sin antes validar la fotografía del aspirante."),
	
	// ================================== DOCUMENTOS ==================================
	DOCUMENTO_GUARDADO("Ocurrió un error al guardar el documento en el servidor remoto."),
	DOCUMENTO_ELIMINADO("Ocurrió un error al eliminar el documento en el servidor remoto."),
	DOCUMENTO_DESCARGADO("Ocurrió un error al descargar el documento, intentalo más tarde."),
	
	// ================================== RESPUESTAS ==================================
	RESPUESTA_ACEPTADA("Ocurrió un error al aceptar la respuesta."),
	RESPUESTA_GUARDADA("Ocurrió un error al guardar la respuesta."),
	RESPUESTA_RECHAZADA("Ocurrió un error al rechazar la respuesta."),
	RESPUESTA_ELIMINADA("Ocurrió un error al eliminar la respuesta."),
	RESPUESTA_YA_ACEPTADA("No puedes actualizar o eliminar un documento el cual ya ha sido aceptado."),
	
	// ================================== TAREAS PROGRAMADAS ==================================
	TAREA_PROGRAMADA_DETENIDA("Ocurrió un error al detener la tarea programada."),
	TAREA_PROGRAMADA_INICIADA("Ocurrió un error al iniciar la tarea programada."),
	TAREA_PROGRAMADA_REINICIADA("Ocurrió un error al reiniciar la tarea programada.");

	private String message;
	public static String ESTATUS = "error";
	
	public static MsgError getByMessage(String message) {
		for(MsgError me : MsgError.values()) {
			if(me.message.equalsIgnoreCase(message))
				return me;
		}
		
		return null;
	}
	
	public String toString() {
		return this.message;
	}
}