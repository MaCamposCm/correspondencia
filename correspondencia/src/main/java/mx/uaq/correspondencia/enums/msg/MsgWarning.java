package mx.uaq.correspondencia.enums.msg;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Getter
@AllArgsConstructor
public enum MsgWarning {
	
	// ================================== NOTICIAS ==================================
	NOTICIA_EVENTO_DUPLICADO("No puedes usar el mismo evento dos veces."),
	
	// ================================== USUARIOS ==================================
	USUARIO_ROLES_MINIMOS("No puede dejar sin roles a un usuario."),
	USUARIO_CORREO_PERSONAL_DIFERENTE("Los correos que intenta ingresar no son iguales."),
	USUARIO_TELEFONO_DIFERENTE("Los números de teléfono que intenta ingresar no son iguales.");
	
	private String message;
	public static String ESTATUS = "warning";
}