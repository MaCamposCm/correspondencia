package mx.uaq.correspondencia.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Getter
@AllArgsConstructor
public enum EnumRolUsuario {
	
	ROLE_ADMINISTRADOR(1L,"ROLE_ADMINISTRADOR","ADMINISTRADOR"),
	ROLE_VALIDADOR_SOLICITUD(2L,"ROLE_VALIDADOR_SOLICITUD","VALIDADOR DE SOLICITUD"),
	ROLE_VALIDADOR_FOTOGRAFIA(3L,"ROLE_VALIDADOR_FOTOGRAFIA","VALIDADOR DE FOTOGRAFÍA"),
	ROLE_ASPIRANTE(4L, "ROLE_ASPIRANTE", "ASPIRANTE");
	
	private Long idRol;
	private String nombre;
	private String descripcion;
}