package mx.uaq.correspondencia.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Getter
@AllArgsConstructor
public enum EnumTipoUsuario {
	
	ASPIRANTE("ASPIRANTE"),
	TRABAJADOR("TRABAJADOR");
	
	private String tipo;
}