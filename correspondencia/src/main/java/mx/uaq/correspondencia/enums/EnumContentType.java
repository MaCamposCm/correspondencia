package mx.uaq.correspondencia.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 28 mar. 2021
 *
 */

@Getter
@AllArgsConstructor
public enum EnumContentType {

	PDF("pdf", "application/pdf"),
	XLS("xls", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
	XLSX("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	
	private String extension;
	private String contentType;
	
	public static String findOneByExtension(String extension) {
		for(EnumContentType ct : EnumContentType.values()) {
			if(ct.getExtension().equalsIgnoreCase(extension))
				return ct.getContentType();
		}
		
		return null;
	}
}	