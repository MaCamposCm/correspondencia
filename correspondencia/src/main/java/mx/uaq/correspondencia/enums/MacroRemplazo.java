package mx.uaq.correspondencia.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 26 mar. 2021
 *
 */

@Getter
@AllArgsConstructor
public enum MacroRemplazo {

	ANIO("{anio}"),
	LIGA("{liga}"),
	FECHA("{fecha}"),
	ESTATUS("{estatus}"),
	DOCUMENTOS("{documentos}"),
	EXPEDIENTE("{expediente}"),
	OBSERVACIONES("{observaciones}"),
	FECHA_REGISTRO("{fechaRegistro}"),
	NOMBRE_USUARIO("{nombreUsuario}"),
	FECHA_HORA_FIN("{fechaHoraInicio}"),
	FECHA_HORA_INICIO("{fechaHoraFin}"),
	FECHA_PARA_ENTREGA("{fechaParaEntrega}"),
	REGISTROS_MIGRADOS("{registrosMigrados}"),
	DOCUMENTOS_MIGRADOS("{documentosMigrados}"),
	REGISTROS_ELIMINADOS("{registrosEliminados}"),
	FOTOGRAFIAS_MIGRADAS("{fotografiasMigradas}");
	
	private String macro;
}