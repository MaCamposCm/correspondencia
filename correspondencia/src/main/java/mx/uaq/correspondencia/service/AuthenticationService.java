package mx.uaq.correspondencia.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.uaq.correspondencia.dao.security.AuthenticationDAO;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Component
public class AuthenticationService {

@Autowired private AuthenticationDAO authenticationDAO;
	
	public Boolean validCredentials(final Object principal, final Object credentials, final String tipo) {
		return authenticationDAO.validaUsuario(principal.toString(), credentials.toString(), tipo);	
	}
	
	public Boolean preValidCredentials(final Object principal, final Object credentials, String tipo) {
		return authenticationDAO.preValidaUsuario(principal.toString(), credentials.toString(), tipo);	
	}
}