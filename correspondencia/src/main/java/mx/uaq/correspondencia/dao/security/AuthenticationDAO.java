package mx.uaq.correspondencia.dao.security;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Repository;

import mx.uaq.correspondencia.dao.UsuarioDAO;
import mx.uaq.correspondencia.enums.EnumEstatusUsuario;
import mx.uaq.correspondencia.enums.EnumTipoUsuario;
import mx.uaq.correspondencia.model.Usuario;


/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Repository
@Profile(value = {"prod"})
public class AuthenticationDAO {
	
	@Autowired private UsuarioDAO usuarioDAO;
	@Autowired private JdbcTemplate jdbcTemplate;
	@Autowired private SessionRegistry sessionRegistry;
	
	public Boolean validaUsuario(String clave, String nip, String tipo) {
	
		
		Usuario usuario = usuarioDAO.findOneByClaveAndTipo(clave, tipo);
		
		if (usuario != null) {	
			if(usuario.getEstatus().equals(EnumEstatusUsuario.INACTIVO.getEstatus())) {
				throw new DisabledException("Authentication Service Reply.");
			} else {
				return validaNip(clave, nip, (tipo.equals(EnumTipoUsuario.ASPIRANTE.getTipo())) ? "E" : "D");
			}
		}
		
		return false;
	}

	private Boolean validaNip(final String clave, final String nip, String tipo) {
		String result = jdbcTemplate.execute(new CallableStatementCreator() {
			
			@Override
			public CallableStatement createCallableStatement(Connection con) throws SQLException {
				CallableStatement cs = con.prepareCall("{? = call VALIDANIP(?, ?, ?)}");
				cs.registerOutParameter(1, Types.VARCHAR);
				cs.setString(2, clave);
				cs.setString(3, nip);
				cs.setString(4, tipo);
				return cs;
			}
		}, new CallableStatementCallback<String>() {

			@Override
			public String doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
				cs.execute();
				String result = cs.getString(1);
				
				return result;
			}
		});
		
		return result.toLowerCase().equalsIgnoreCase("ok") ? true : false;
	}

	public Boolean preValidaUsuario(String clave, String nip, String tipo) {
		return validaNip(clave, nip, (tipo.equals(EnumTipoUsuario.ASPIRANTE.getTipo())) ? "E" : "D");
	}
}