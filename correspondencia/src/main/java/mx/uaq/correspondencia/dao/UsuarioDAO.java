package mx.uaq.correspondencia.dao;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import mx.uaq.correspondencia.model.Usuario;



@Repository
public class UsuarioDAO extends AbstractJPADAO<Usuario> {
	
	public UsuarioDAO () {
		super(Usuario.class);
	}
	
	public Usuario findOneByCorreoPersonal(String correoPersonal) {
		try {
			final TypedQuery<Usuario> typedQuery = this.getEntityManager().createNamedQuery("Usuario.findOneByCorreoPersonal", Usuario.class);
			typedQuery.setParameter("correoPersonal", correoPersonal);
			
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}

	public Usuario findOneByClaveAndTipo(String clave, String tipo) {
		try {
			final TypedQuery<Usuario> typedQuery = this.getEntityManager().createNamedQuery("Usuario.findOneByClaveAndTipo", Usuario.class);
			typedQuery.setParameter("clave", clave);
			typedQuery.setParameter("tipo", tipo);
			
			return typedQuery.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}
	


	
	
}