package mx.uaq.correspondencia.dao.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Repository;

import mx.uaq.correspondencia.dao.UsuarioDAO;
import mx.uaq.correspondencia.enums.EnumEstatusUsuario;
import mx.uaq.correspondencia.model.Usuario;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Repository
@Profile(value = {"local", "dev", "qa", "pre"})
public class AuthenticationFakeDAO extends AuthenticationDAO {

	@Autowired private UsuarioDAO usuarioDAO;
	@Autowired private SessionRegistry sessionRegistry;
	
	private static final String PASSWORD = "uaq";

	@Override
	public Boolean validaUsuario(String clave, String nip, String tipo) {

		
		Usuario usuario = usuarioDAO.findOneByClaveAndTipo(clave, tipo);
		
		if (usuario != null) {	
			if(usuario.getEstatus().equals(EnumEstatusUsuario.INACTIVO.getEstatus())) {
				throw new DisabledException("Authentication Service Reply.");
			} else {
				return nip.equalsIgnoreCase(PASSWORD);
			}
		}
		
		return false;
	}

	@Override
	public Boolean preValidaUsuario(String clave, String nip, String tipo) {
		return nip.equalsIgnoreCase(PASSWORD);
	}
}
