package mx.uaq.correspondencia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@NoArgsConstructor
public class AbstractJPADAO<T> {
	
	private Class<T> clazz;
	
	@Getter 
	@PersistenceContext
	private EntityManager entityManager;
	
	public AbstractJPADAO(Class<T> clazz) {
		this.clazz = clazz;
	}
	
	public T findOneById(long id) {
		return entityManager.find(clazz, id);
	}
	
	public List<T> findAll(){
		return entityManager.createQuery("from " + clazz.getName(),clazz).getResultList();
	} 
	
	public void create(T entity) {
		entityManager.persist(entity);
	}
	
	public T update(T entity) {
		return entityManager.merge(entity);
	}

	public void delete(T entity) {
		entityManager.remove(entity);
	}
	
	public void deleteById(long id) {
		T entity = findOneById(id);
		entityManager.remove(entity);
	}
}