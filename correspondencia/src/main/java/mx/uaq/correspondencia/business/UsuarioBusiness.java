package mx.uaq.correspondencia.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.uaq.correspondencia.dao.UsuarioDAO;
import mx.uaq.correspondencia.enums.EnumEstatusUsuario;
import mx.uaq.correspondencia.enums.msg.MsgError;
import mx.uaq.correspondencia.exception.UaqException;
import mx.uaq.correspondencia.model.Usuario;
import mx.uaq.correspondencia.service.AuthenticationService;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Component
public class UsuarioBusiness {
	
	@Autowired private UsuarioDAO usuarioDAO;

	@Autowired private AuthenticationService authenticationDAO;

	@Value("${max.intentos.logueo}") private Integer MAX_INTENTOS;
	
//	
//	
//	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class) 
//	public MsgTransaction createOrUpdate(UsuarioVO usuario, Usuario sesion) {
//		if(usuario.getId() != null) {			
//			Usuario usuarioDB = usuarioDAO.findOneById(usuario.getId());
//			
//			if(usuarioDB != null) {
//				if((!CustomUtil.listIsNotBlank(usuario.getRolList())) && (!(usuario.getRolList().size() >= 1)))
//					return new MsgTransaction(MsgWarning.USUARIO_ROLES_MINIMOS);
//			
//				usuarioDB.setCorreoPersonal(usuario.getCorreoPersonal());
//				usuarioDB.setTelefono(usuario.getTelefono());
//				
//				rolUsuarioBusiness.createOrUpdate(usuario.getRolList(), usuarioDB.getId());
//				usuarioEscuelaBusiness.createOrUpdate(usuario.getEscuelaList(), usuarioDB.getId());
//				
//				return new MsgTransaction(MsgSuccess.USUARIO_ACTUALIZADO);
//			}
//		} else {
//			if((!CustomUtil.listIsNotBlank(usuario.getRolList())) && (!(usuario.getRolList().size() >= 1)))
//				return new MsgTransaction(MsgWarning.USUARIO_ROLES_MINIMOS);
//			
//			Usuario u = new Usuario(usuario);
//			u.setFallos(0);
//			u.setEstatus(EnumEstatusUsuario.ACTIVO.getEstatus());
//			
//			if(usuario.getTipo().equals(EnumTipoUsuario.ASPIRANTE.getTipo())) {
//				Aspirante aspirante = aspiranteDAO.findOneByExpediente(usuario.getClave());
//				
//				if(aspirante != null) {
//					u.setNombre(aspirante.getNombre());
//					u.setApellidoPaterno(aspirante.getApellidoPaterno());
//					u.setApellidoMaterno(aspirante.getApellidoMaterno());
//					u.setNombreCompleto((StringUtils.isNotBlank(aspirante.getApellidoMaterno())) ? 
//							(aspirante.getNombre() + " " + aspirante.getApellidoPaterno() + " " + aspirante.getApellidoMaterno()) : 
//							(aspirante.getNombre() + " " + aspirante.getApellidoPaterno()));
//					u.setNacionalidad((aspirante.getNacionalidad().equals("MEX")) ? "MEXICANA" : "EXTRANJERA");
//					u.setSexo((aspirante.getSexo().equals("F")) ? "FEMENINO" : "MASCULINO");
//					u.setEstatusFotografia(EnumEstatusUsuarioFotografia.PENDIENTE.getEstatus());
//					usuarioDAO.create(u);
//				}
//			}
//			if(usuario.getTipo().equals(EnumTipoUsuario.TRABAJADOR.getTipo())) {
//				Empleado empleado = empleadoDAO.findOneByClave(usuario.getClave());
//				
//				if(empleado != null) {
//					u.setNombre(empleado.getNombre());
//					u.setApellidoPaterno(empleado.getApPaterno());
//					u.setApellidoMaterno(empleado.getApMaterno());
//					u.setNombreCompleto((StringUtils.isNotBlank(empleado.getApMaterno())) ? 
//							(empleado.getNombre() + " " + empleado.getApPaterno() + " " + empleado.getApMaterno()) : 
//							(empleado.getNombre() + " " + empleado.getApPaterno()));
//					u.setEstatusFotografia(EnumEstatusUsuarioFotografia.NO_APLICA.getEstatus());
//				}
//				
//				usuarioDAO.create(u);
//			}
//			
//			rolUsuarioBusiness.createOrUpdate(usuario.getRolList(), u.getId());
//			usuarioEscuelaBusiness.createOrUpdate(usuario.getEscuelaList(), u.getId());
//			
//			return new MsgTransaction(MsgSuccess.USUARIO_CREADO);
//		}
//		
//		return new MsgTransaction(MsgError.USUARIO_REGISTRADO);
//	}
//	
//	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
//	public MsgTransaction unlock(Long idUsuario) {
//		Usuario usuario = usuarioDAO.findOneById(idUsuario);
//		
//		if(usuario != null) {
//			if(usuario.getEstatus().equals(EnumEstatusUsuario.ACTIVO.getEstatus()))
//				return new MsgTransaction(MsgNotification.USUARIO_ACTIVADO);
//			
//			usuario.setEstatus(EnumEstatusUsuario.ACTIVO.getEstatus());
//			usuario.setFallos(0);
//			usuarioDAO.update(usuario);
//			
//			return new MsgTransaction(MsgSuccess.USUARIO_ACTIVADO);
//		}
//		
//		return new MsgTransaction(MsgError.USUARIO_DESBLOQUEADO);
//	}
//	
//	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
//	public MsgTransaction disabled(Long idUsuario) {
//		Usuario usuario = usuarioDAO.findOneById(idUsuario);
//		
//		if(usuario != null) {
//			if(usuario.getEstatus().equals(EnumEstatusUsuario.INACTIVO.getEstatus()))
//				return new MsgTransaction(MsgNotification.USUARIO_INACTIVADO);
//			
//			usuario.setEstatus(EnumEstatusUsuario.INACTIVO.getEstatus());
//			usuarioDAO.update(usuario);
//			
//			return new MsgTransaction(MsgSuccess.USUARIO_INACTIVADO);
//		}
//		
//		return new MsgTransaction(MsgError.USUARIO_INACTIVADO);
//	}
//	
//	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
//	public MsgTransaction changeTelefono(TelefonoVO telefono) {
//		Usuario usuario = usuarioDAO.findOneById(telefono.getIdUsuario());
//		
//		if(usuario != null && StringUtils.isNotBlank(telefono.getTelefono()) && StringUtils.isNotBlank(telefono.getConfirmacionTelefono())) {
//			if(!(telefono.getTelefono().equals(telefono.getConfirmacionTelefono())))
//				return new MsgTransaction(MsgWarning.USUARIO_TELEFONO_DIFERENTE);
//			
//			usuario.setTelefono(telefono.getTelefono());
//			usuarioDAO.update(usuario);
//			
//			return new MsgTransaction(MsgSuccess.USUARIO_TELEFONO_ACTUALIZADO, new UsuarioVO(usuario));
//		}
//		
//		return new MsgTransaction(MsgError.USUARIO_TELEFONO_ACTUALIZADO);
//	}
//	
//	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
//	public MsgTransaction changeCorreoPersonal(CorreoPersonalVO correo) {
//		Usuario usuario = usuarioDAO.findOneById(correo.getIdUsuario());
//		
//		if(usuario != null && StringUtils.isNotBlank(correo.getCorreoPersonal()) && StringUtils.isNotBlank(correo.getConfirmacionCorreo())) {
//			if(!(correo.getCorreoPersonal().equals(correo.getConfirmacionCorreo())))
//				return new MsgTransaction(MsgWarning.USUARIO_CORREO_PERSONAL_DIFERENTE);
//			
//			usuario.setCorreoPersonal(correo.getCorreoPersonal());
//			usuarioDAO.update(usuario);
//			
//			return new MsgTransaction(MsgSuccess.USUARIO_CORREO_PERSONAL_ACTUALIZADO, new UsuarioVO(usuario));
//		}
//		
//		return new MsgTransaction(MsgError.USUARIO_CORREO_ACTUALIZADO);
//	}
//	
//	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class) 
//	public void createAtLogin(String clave, String password, String tipo) {
//		Usuario usuario = usuarioDAO.findOneByClaveAndTipo(clave, tipo);
//		
//		if(usuario == null) {
//			if(!StringUtils.isNumeric(clave))
//				return;
//			
//			if(authenticationDAO.preValidCredentials(clave, password, tipo)) {
//				Periodo periodo = null;
//				boolean noActivePeriod = true;
//				List<AspiranteDetalle> aspiranteDetalles = null;
//				Aspirante aspirante = aspiranteDAO.findOneByExpediente(clave);
//				Configuracion configuracionPeriodo = configuracionBusiness.findOrInitializeOneByClave(EnumConfiguracion.PERIODOS_PERMITIDOS.getClave());
//				Configuracion configuracion = configuracionBusiness.findOrInitializeOneByClave(EnumConfiguracion.FACULTADES_PERMITIDAS.getClave());
//				
//				if(configuracionPeriodo == null)
//					throw new AspirantePeriodoException("No existe periodo");
//					
//				if(configuracion != null) {
//					aspiranteDetalles = aspiranteDetalleDAO.findOneByExpedienteAndFacultadesAndPeriodo(clave, configuracion.getValor(), configuracionPeriodo.getValor());
//				} else {
//					aspiranteDetalles = aspiranteDetalleDAO.findOneByExpedienteAndPeriodo(clave, configuracionPeriodo.getValor());
//				}
//				
//				if(aspirante != null) {
//					if(aspirante.getNombre() == null || aspirante.getApellidoPaterno() == null)
//						throw new AspiranteInformacionIncompletaException("Tiene informacion incpmpleta");
//					
//					noActivePeriod = true;
//					for(AspiranteDetalle ad : aspiranteDetalles) {
//						periodo = periodoBusiness.getCurrentForSolicitud(ad.getNivel());
//						
//						if(periodo != null)
//							noActivePeriod = false;
//					}
//					
//					if(noActivePeriod)
//						throw new AspirantePeriodoException("No existe periodo");
//					
//					if(usuarioDAO.findOneByCorreoPersonal(aspirante.getCorreo()) != null)
//						throw new InformacionRegistradaException("Correo duplicado");
//					
//					usuario = new Usuario(aspirante);
//					usuarioDAO.create(usuario);
//					
//					rolUsuarioDAO.create(new RolUsuario(EnumRolUsuario.ROLE_ASPIRANTE.getIdRol(), usuario.getId()));
//				}
//			}					
//		}
//	}
//	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void processAuthenticationFailure(String clave, String tipo) {
		try { 
			Usuario usuario = usuarioDAO.findOneByClaveAndTipo(clave, tipo);
			
			if(usuario != null) {
				if(usuario.getFallos() == (MAX_INTENTOS - 1)) {
					usuario.setFallos(usuario.getFallos() + 1);
					usuario.setEstatus(EnumEstatusUsuario.BLOQUEADO.getEstatus());
				} else {
					usuario.setFallos(usuario.getFallos() + 1);
				}
				usuarioDAO.update(usuario);
			}
		}catch (Exception e) {
			throw new UaqException(MsgError.USUARIO_BLOQUEADO, e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class) 
	public Usuario resetFailures(String clave, String tipo) {
		try { 
			Usuario usuario = usuarioDAO.findOneByClaveAndTipo(clave, tipo);
			
			if(usuario != null && usuario.getFallos() != 0) {
				usuario.setFallos(0);
				usuarioDAO.update(usuario);
			}
			
			return usuario;
		} catch (Exception e) {
			throw new UaqException(MsgError.USUARIO_NUMERO_INTENTOS, e);
		}
	}
	
//	public DataResultFilter<UsuarioVO> getUsuariosByFilter(UsuarioFilter filter) {
//		List<UsuarioVO> listaUsuarios = new ArrayList<UsuarioVO>();
//		DataResultFilter<UsuarioVO> dataResultFilter = new DataResultFilter<UsuarioVO>();
//		
//		for(Usuario u : usuarioDAO.getUsuariosByFilter(filter))
//			listaUsuarios.add(new UsuarioVO(u));
//		
//		dataResultFilter.setData(listaUsuarios);
//		dataResultFilter.setRecordsTotal(usuarioDAO.countUsuariosByFilter(filter));
//		
//		return dataResultFilter;
//	}
}