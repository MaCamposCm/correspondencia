package mx.uaq.correspondencia.util;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import mx.uaq.correspondencia.enums.EnumTipoUsuario;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

public class CustomUtil {
	
	public static void downlodArchive(final HttpServletResponse response, 
			final InputStream inputStream, final String contentType, final String nombreArchivo, final boolean isViewMode) throws Exception {
		response.setContentType(contentType);
		response.setHeader("Content-Disposition", ((isViewMode) ? "inline; " : "attachment; ") + "filename=" + CustomUtil.createAllowFileName(nombreArchivo));
		
		IOUtils.copy(inputStream, response.getOutputStream());
		response.flushBuffer();
		inputStream.close();
	}
	
	public static String createAllowFileName(String fileName) {
		StringBuilder fileNameResponse = new StringBuilder();
		
		fileNameResponse.append(FilenameUtils.getBaseName(fileName).replace(",", ""));
		fileNameResponse.append("." + FilenameUtils.getExtension(fileName));
		
		return fileNameResponse.toString();
	}
	
	public static String dateFormatToText(Date date) {
		try {
			String[] dateAux = new SimpleDateFormat("dd/MM/yyyy").format(date).split("/");
			StringBuilder dateResponse = new StringBuilder();
			dateResponse.append(dateAux[0] + " de " + getMonthLikeText(Integer.parseInt(dateAux[1])) + " de " + dateAux[2]);
			
			return dateResponse.toString();
		} catch(Exception e) {
			return null;
		}
	}
	
	public static String getMonthLikeText(int month) {
		switch(month) {
			case 1:
				return "enero";
			case 2:
				return "febrero";
			case 3:
				return "marzo";
			case 4:
				return "abril";
			case 5:
				return "mayo";
			case 6:
				return "junio";
			case 7:
				return "julio";
			case 8:
				return "agosto";
			case 9:
				return "septiembre";
			case 10:
				return "octubre";
			case 11:
				return "noviembre";
			case 12:
				return "diciembre";
			default:
				return null;
		}
	}
	
	public static boolean isSameOrBefore(Date start, Date end, boolean useHourAndMinutes) {
		try {
			SimpleDateFormat fmt = new SimpleDateFormat(useHourAndMinutes ? "dd/MM/yyyy HH:mm" : "dd/MM/yyyy");
			start = fmt.parse(fmt.format(start));
			end = fmt.parse(fmt.format(end));
			
			return (start.equals(end) || start.before(end));
		} catch(Exception e) {
			return false;
		}
	}
	
	public static Date createDateHour(String date, String hour) {
		try {
			String dateComplete = date + " " + convertTo24Hours(hour);
			
			return new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(dateComplete);
		} catch(Exception e) {
			return null;
		}
	}
	
	public static String convertTo24Hours(String hour) {
		String[] hourSplited = hour.split(" ");
		StringBuilder hourResponse = new StringBuilder();
		Integer hours = Integer.parseInt(hourSplited[0].split(":")[0]);
		Integer minutes = Integer.parseInt(hourSplited[0].split(":")[1]);
		
		if(hourSplited[1].equalsIgnoreCase("AM") && hours == 12)
			hours = 0;
		if(hourSplited[1].equalsIgnoreCase("PM") && hours != 12)
			hours = hours + 12;
		
		hourResponse.append(hours + ":" + minutes);
		
		return hourResponse.toString();
	}
	
	public static boolean listIsNotBlank(final List<?> objectsList) {
		if( objectsList != null && !(objectsList.isEmpty()) && objectsList.size() > 0 ) {
			return true;
		}
		return false;
	}

	public static String getTipoUsuarioFromRequest(HttpServletRequest request) {
		String tipo = request.getParameter("tipo");
		
		if(tipo.equals("1"))
			return EnumTipoUsuario.TRABAJADOR.getTipo();
		
		return tipo;
	}

	public static String getStringFromListOfString(List<String> objeto) {
		StringBuilder response = new StringBuilder();
		
		for(String string : objeto) {
			if(StringUtils.isNotBlank(response))
				response.append(", ");
				
			response.append("'" + string + "'");
		}
		
		return response.toString();
	}
}