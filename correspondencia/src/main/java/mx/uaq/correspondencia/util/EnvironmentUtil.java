package mx.uaq.correspondencia.util;

import java.util.List;

import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 26 mar. 2021
 *
 */

@Component
public class EnvironmentUtil implements EnvironmentAware {

	private Environment env;

	@Override
	public void setEnvironment(Environment environment) {
		env = environment;
	}

	public String getServerUrl() {
		switch (env.getActiveProfiles()[0]) {
			case "prod":
				return "http://www.uaq.mx/portalposgrado";
			case "qa":
				return "http://148.220.220.111:7001/portalposgrado_qa/";
			case "dev":
				return "http://148.220.220.111:7001/portalposgrado_dev/";
			case "pre":
				return "https://comunidad3.uaq.mx:8012/portalposgrado_pre/";
		}

		return null;
	}

	public String[] getCorreosByEnviroment(List<String> emailsList) {
		switch (env.getActiveProfiles()[0]) {
			case "prod":
				String[] emails = new String[emailsList.size()];
	
				return emailsList.toArray(emails);
			case "dev":
				return new String[] { "efren.pacheco@uaq.mx" };
			case "qa": case "pre":
				return new String[] { "efren.pacheco@uaq.mx", "miguel.martinez.barcenas@uaq.mx",
						"jose.natanael.garcia@uaq.mx", "joaquin.aguilar@uaq.mx" };
		}

		return null;
	}
}