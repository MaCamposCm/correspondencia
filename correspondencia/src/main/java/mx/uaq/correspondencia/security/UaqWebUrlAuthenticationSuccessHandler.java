package mx.uaq.correspondencia.security;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import mx.uaq.correspondencia.business.UsuarioBusiness;
import mx.uaq.correspondencia.enums.EnumRolUsuario;
import mx.uaq.correspondencia.model.Usuario;
import mx.uaq.correspondencia.util.CustomUtil;
import mx.uaq.correspondencia.configuration.SecurityConfiguration;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Component
public class UaqWebUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	
	@Autowired private UsuarioBusiness usuarioBusiness;
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		handler(request, response, authentication);
		clearAuthenticationAttributes(request);	
	}

	private void handler(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException {
		Usuario usuario = resetIntentosFallidos(request);
		String targetUrl = determineTargetUrl(usuario, authentication);
		
		if(response.isCommitted())
			return;
		
		redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	private Usuario resetIntentosFallidos(HttpServletRequest request) {
		return usuarioBusiness.resetFailures(request.getParameter("username"), CustomUtil.getTipoUsuarioFromRequest(request));
	}

	private String determineTargetUrl(Usuario usuario, Authentication authentication) {
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		
		for (GrantedAuthority grantedAuthority : authorities) {
			if (EnumRolUsuario.ROLE_ADMINISTRADOR.getNombre().equals(grantedAuthority.getAuthority())) {
				return "/administrador/dashboard?idRol=1&inicioSesion=1";
			} 
		}

		return SecurityConfiguration.LOGOUT_PERMISO_DENEGADO;
	}
	
	protected void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if(session == null)
			return;
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}
}