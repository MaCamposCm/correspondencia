package mx.uaq.correspondencia.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class UaqUserDetails extends User {
	
	private static final long serialVersionUID = 1L;
	private Long idUsuario;
	
	public UaqUserDetails(String username, String password, boolean enabled, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, true, true, accountNonLocked, authorities);
	}
}