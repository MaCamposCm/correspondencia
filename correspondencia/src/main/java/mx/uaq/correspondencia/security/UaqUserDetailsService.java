package mx.uaq.correspondencia.security;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import mx.uaq.correspondencia.dao.UsuarioDAO;
import mx.uaq.correspondencia.enums.EnumEstatusUsuario;
import mx.uaq.correspondencia.model.Rol;
import mx.uaq.correspondencia.model.Usuario;
import mx.uaq.correspondencia.util.CustomUtil;



public class UaqUserDetailsService implements UserDetailsService {
	
	@Autowired private UsuarioDAO usuarioDAO;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpServletRequest request = attributes.getRequest();
		
		Usuario usuario = usuarioDAO.findOneByClaveAndTipo(username, CustomUtil.getTipoUsuarioFromRequest(request));
		
		if(usuario == null)
			return null;
		
		return createUserDetails(username, usuario, createGrantedAuthorities(usuario));
	}

	private UserDetails createUserDetails(String username, Usuario usuario,
			List<GrantedAuthority> authorities) {
		UaqUserDetails uaqUserDetails;
		
		Boolean accountEnable = true;
		EnumEstatusUsuario estatus = EnumEstatusUsuario.valueOf(usuario.getEstatus());
		
		if(!(estatus.equals(EnumEstatusUsuario.BLOQUEADO)))
			uaqUserDetails = new UaqUserDetails(username + " " + usuario.getClave(), "noPassword", accountEnable, true, authorities);
		else
			uaqUserDetails = new UaqUserDetails(username, "", true, false, authorities);
		
		uaqUserDetails.setIdUsuario(usuario.getId());
		
		return uaqUserDetails;
	}

	private List<GrantedAuthority> createGrantedAuthorities(Usuario usuario) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		for(Rol rol : usuario.getRoles())
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMINISTRADOR"));
		
		return authorities;
	}
}