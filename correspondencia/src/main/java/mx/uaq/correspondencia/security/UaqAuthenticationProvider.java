package mx.uaq.correspondencia.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import mx.uaq.correspondencia.business.UsuarioBusiness;
import mx.uaq.correspondencia.service.AuthenticationService;
import mx.uaq.correspondencia.util.CustomUtil;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

public class UaqAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
	
	@Autowired private UsuarioBusiness usuarioBusiness;
	@Autowired private UserDetailsService userDetailsService;
	@Autowired private AuthenticationService authenticationService;
	
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpServletRequest request = attributes.getRequest();
		String tipo = CustomUtil.getTipoUsuarioFromRequest(request);
		Boolean responseNipAuthentication = authenticationService.validCredentials(authentication.getPrincipal().toString().trim(), authentication.getCredentials().toString().trim(), tipo);
		
		if(!responseNipAuthentication) {
			usuarioBusiness.processAuthenticationFailure(authentication.getPrincipal().toString().trim(), tipo);
			throw new BadCredentialsException("Authentication Service Reply: " + responseNipAuthentication);
		}
	}
	
	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		UserDetails user = userDetailsService.loadUserByUsername(username);
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpServletRequest request = attributes.getRequest();
		String tipo = CustomUtil.getTipoUsuarioFromRequest(request);
		
		if(user == null) {
			
//			usuarioBusiness.createAtLogin(username, authentication.getCredentials().toString(), tipo);
			user = userDetailsService.loadUserByUsername(username);
			
			if(user != null)
				return user;
			else
				throw new BadCredentialsException("Authentication Service Reply.");
		}
		
		return user;
	}
}
