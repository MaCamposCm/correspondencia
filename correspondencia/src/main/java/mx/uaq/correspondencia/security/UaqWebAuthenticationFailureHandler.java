package mx.uaq.correspondencia.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

import mx.uaq.correspondencia.exception.login.MaxUsersInLoginException;


@Configuration
public class UaqWebAuthenticationFailureHandler implements AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, 
			AuthenticationException exception) throws IOException, ServletException {
		request.setAttribute("userMessage", getUserMessage(exception));
		request.getRequestDispatcher("/login").forward(request, response);
	}	
	
	private String getUserMessage(final AuthenticationException exception) {
		String userMessage = "Tus datos son incorrectos o tu escuela/facultad está fuera de fecha.";
		
		if(exception instanceof BadCredentialsException)
			userMessage = "Tus datos son incorrectos o tu escuela/facultad está fuera de fecha.";
		else if(exception instanceof LockedException)
			userMessage = "Usuario bloqueado.";
		else if(exception instanceof DisabledException)
			userMessage = "Usuario eliminado temporalmente.";
		else if(exception instanceof AccountExpiredException)
			userMessage = "La cuenta ha expirado.";
		else if(exception instanceof CredentialsExpiredException)
			userMessage = "Contraseña expirada.";
		else if(exception instanceof SessionAuthenticationException)
			userMessage = "Ya cuenta con una sesión activa";
		else if(exception instanceof MaxUsersInLoginException)
			userMessage = "Lo sentimos el sistema esta saturado, intentelo en otro momento.";
		
		return userMessage;
	}
}