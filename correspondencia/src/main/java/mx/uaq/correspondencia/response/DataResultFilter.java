package mx.uaq.correspondencia.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 26 mar. 2021
 *
 */

@Getter
@Setter
public class DataResultFilter<T> {

	private long recordsTotal;
	private List<T> data = new ArrayList<>();
}