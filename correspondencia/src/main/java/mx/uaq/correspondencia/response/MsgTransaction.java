package mx.uaq.correspondencia.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import mx.uaq.correspondencia.enums.msg.MsgError;
import mx.uaq.correspondencia.enums.msg.MsgNotification;
import mx.uaq.correspondencia.enums.msg.MsgSuccess;
import mx.uaq.correspondencia.enums.msg.MsgWarning;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MsgTransaction {

	private Object data;
	private String status;
	private String result;
	private Integer responseCode;
	
	public MsgTransaction(MsgSuccess msg) {
		super();
		this.status = MsgSuccess.ESTATUS;
		this.result = msg.getMessage();
	}
	
	public MsgTransaction(MsgSuccess msg, Object data) {
		super();
		this.data = data;
		this.status = MsgSuccess.ESTATUS;
		this.result = msg.getMessage();
	}
	
	public MsgTransaction(MsgError msg) {
		super();
		this.status = MsgError.ESTATUS;
		this.result = msg.getMessage();
	}
	
	public MsgTransaction(MsgError msg, Object data) {
		super();
		this.data = data;
		this.status = MsgError.ESTATUS;
		this.result = msg.getMessage();
	}
	
	public MsgTransaction(MsgWarning msg) {
		super();
		this.status = MsgWarning.ESTATUS;
		this.result = msg.getMessage();
	}
	
	public MsgTransaction(MsgWarning msg, Object data) {
		super();
		this.data = data;
		this.status = MsgWarning.ESTATUS;
		this.result = msg.getMessage();
	}
	
	public MsgTransaction(MsgNotification msg) {
		super();
		this.status = MsgNotification.ESTATUS;
		this.result = msg.getMessage();
	}
	
	public MsgTransaction(MsgNotification msg, Object data) {
		super();
		this.data = data;
		this.status = MsgNotification.ESTATUS;
		this.result = msg.getMessage();
	}
}