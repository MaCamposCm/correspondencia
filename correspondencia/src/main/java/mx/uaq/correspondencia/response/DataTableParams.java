package mx.uaq.correspondencia.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 26 mar. 2021
 *
 */

@Getter
@Setter
@ToString
public class DataTableParams {

	private int start;
	private int length;
	private String draw;
	
	public int getOffset() {
		return this.start;
	}
	
	public int getLimit() {
		return this.length;
	}
}