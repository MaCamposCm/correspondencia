package mx.uaq.correspondencia.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 26 mar. 2021
 *
 */

@Getter
@Setter
public class DataTableResponse {

	private long recordsTotal = 100000l;
	private long recordsFiltered = 0;
	private int draw;
	private List<? extends Object> data = new ArrayList<>();
	
	public DataTableResponse(final String draw) {
		this.draw = Integer.parseInt(draw);
	}
	
	public DataTableResponse(final DataTableParams tableParams, final List<? extends Object> list,
			final long recordsFiltered) {
		this(tableParams.getDraw());
		this.data = list;
		this.recordsFiltered = recordsFiltered;
		if(this.data.size() < tableParams.getLimit()) {
			this.recordsTotal = (long) tableParams.getOffset() + this.data.size();
		}
	}
}