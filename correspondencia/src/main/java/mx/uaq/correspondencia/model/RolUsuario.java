package mx.uaq.correspondencia.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Data
@Entity
@NoArgsConstructor
@Table(name = "ROL_USUARIO")
@NamedQueries({
	@NamedQuery(name = "RolUsuario.findAllByIdUsuario", query = "SELECT ru FROM RolUsuario ru WHERE ru.idUsuario = :idUsuario")
})
public class RolUsuario implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@Column(name = "ID_ROL")
	@Basic(optional = false)
	private Long idRol; 
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ROL", referencedColumnName = "ID", insertable = false, updatable = false, nullable = false)
	private Rol rol;
	
	@Id
	@NotNull
	@Column(name = "ID_USUARIO")
	@Basic(optional = false)
	private Long idUsuario; 
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID", insertable = false, updatable = false, nullable = false)
	private Usuario usuario;
	
	public RolUsuario(Long idRol, Long idUsuario) {
		this.idRol = idRol;
		this.idUsuario = idUsuario;
	}
}