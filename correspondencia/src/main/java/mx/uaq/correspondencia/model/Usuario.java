package mx.uaq.correspondencia.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

@Data
@Entity
@NoArgsConstructor
@Table(name = "USUARIO")
@NamedQueries({
	@NamedQuery(name = "Usuario.findOneByCorreoPersonal", query = "SELECT u FROM Usuario u WHERE u.correoPersonal = :correoPersonal"),
	@NamedQuery(name = "Usuario.findOneByClaveAndTipo", query = "SELECT u FROM Usuario u WHERE u.clave = :clave AND u.tipo = :tipo")
})
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@Column(name = "ID")
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_USUARIO")
	@SequenceGenerator(name = "SEQ_USUARIO", sequenceName = "SEQ_USUARIO", allocationSize = 1)
	private Long id;
	
	@NotNull
	@Size(max = 20)
	@Basic(optional = false)
	@Column(name = "CLAVE", unique = true)
	private String clave;
	
	@NotNull
	@Size(max = 50)
	@Column(name = "NOMBRE")
	@Basic(optional = false)
	private String nombre;
	
	@NotNull
	@Size(max = 50)
	@Basic(optional = false)
	@Column(name = "APELLIDO_PATERNO")
	private String apellidoPaterno;
	
	@Size(max = 50)
	@Basic(optional = false)
	@Column(name = "APELLIDO_MATERNO")
	private String apellidoMaterno;
	
	@NotNull
	@Size(max = 150)
	@Basic(optional = false)
	@Column(name = "NOMBRE_COMPLETO")
	private String nombreCompleto;
		
	@Size(max = 50)
	@Basic(optional = false)
	@Column(name = "CORREO_PERSONAL", unique = true)
	private String correoPersonal;
	
	@Size(max = 50)
	@Basic(optional = false)
	@Column(name = "CORREO_INSTITUCIONAL", unique = true)
	private String correoInstitucional;
	
	@NotNull
	@Size(max = 15)
	@Column(name = "TIPO")
	@Basic(optional = false)
	private String tipo;
	
	@NotNull
	@Basic(optional = false)
	@Column(name = "FALLOS")
	private Integer fallos;
	
	@NotNull
	@Size(max = 20)
	@Column(name = "ESTATUS")
	@Basic(optional = false)
	private String estatus;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "ROL_USUARIO",
		joinColumns = @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID"), 
		inverseJoinColumns = @JoinColumn(name = "ID_ROL", referencedColumnName = "ID"))
	private List<Rol> roles;
	
	
}	