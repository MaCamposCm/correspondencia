package mx.uaq.correspondencia.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.integration.smb.session.SmbSession;
import org.springframework.integration.smb.session.SmbSessionFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.fasterxml.jackson.databind.ObjectMapper;



@Configuration
@EnableScheduling
@Import({ DBConfiguration.class, PersistenceJPAConfiguration.class, PropertiesConfiguration.class })
@ComponentScan({ "mx.uaq.correspondencia.security", "mx.uaq.correspondencia.business", "mx.uaq.correspondencia.dao",
	"mx.uaq.correspondencia.service", "mx.uaq.correspondencia.mail", "mx.uaq.correspondencia.util",
	"mx.uaq.correspondencia.jasper", "mx.uaq.correspondencia.task" })
public class RootConfiguration {
	

	
	@Bean(destroyMethod="shutdown")
	public TaskScheduler taskScheduler() {
	    ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
	    scheduler.setThreadNamePrefix("ThreadPoolTaskScheduler");
	    scheduler.setPoolSize(10);
	    scheduler.initialize();
	    return scheduler;
	}

	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;

	@Bean
	public RestTemplate createRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(60000);
		((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(60000);

		return restTemplate;
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfig() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver getMultipartResolver() {
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setDefaultEncoding("utf-8");

		return resolver;
	}

	@Bean
	public ObjectMapper basicJasonObjectMapper() {
		return new ObjectMapper();
	}

	@Bean
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(dataSource);
	}

}