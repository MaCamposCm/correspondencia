package mx.uaq.correspondencia.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

public class PropertiesConfiguration implements EnvironmentAware {
	
	private Environment env;

	@Override
	public void setEnvironment(Environment environment) {
		env = environment;
	}
	
	@Bean
	public PropertySourcesPlaceholderConfigurer propertySorucePlaceholderConfigurer() {
		PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
		configurer.setLocations(locations());
		
		return configurer;
	}
	
	@Bean
	public Resource[] locations() {
		String[] locations = new String[] {"application"};
		List<Resource> resourceList = new ArrayList<Resource>(locations.length);
		String profileSuffix = createProfileSuffixFromProfile();
		for (String location : locations)
			resourceList.add(new ClassPathResource(String.format("%s%s.properties", location, profileSuffix)));
		
		return resourceList.toArray(new Resource[] {});
	}
	
	private String createProfileSuffixFromProfile() {
		String firstActiveProfile = env.getActiveProfiles()[0];
		
		return firstActiveProfile.equals("prod") ? "" : "_" + firstActiveProfile;
	}
}