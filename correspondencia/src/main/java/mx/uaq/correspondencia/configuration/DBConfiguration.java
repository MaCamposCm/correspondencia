package mx.uaq.correspondencia.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 *
 * UAQ - Universidad Autónoma de Queréraro
 * DITI - Dirección de Innovación y Tecnologías de la información
 * 
 * Autor: Efrén Pacheco Sánchez
 * Versión: 1.0.0
 * Fecha: 25 mar. 2021
 *
 */

public class DBConfiguration {
	
	@Value("${main.db.jdbcUrl}") private String jdbcUrl;
	@Value("${main.db.username}") private String userName;
	@Value("${main.db.password}") private String password;
	@Value("${main.db.driverClassName}") private String dataSource;
	@Value("${main.db.maximumPoolSize}") private Integer maximumPoolsize;
	
	@Bean(name="dataSource",destroyMethod="close")
	public DataSource getDataSourceOracle() {
		HikariConfig config = new HikariConfig();
		config.setDriverClassName(dataSource);
		config.setJdbcUrl(jdbcUrl);
		config.setUsername(userName);
		config.setPassword(password);
		config.setMaximumPoolSize(maximumPoolsize);
		config.setAutoCommit(false);
		
		return new HikariDataSource(config);
	}
	
	public JdbcTemplate jdbcTemplateOracle(@Qualifier("dataSource") DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}
}