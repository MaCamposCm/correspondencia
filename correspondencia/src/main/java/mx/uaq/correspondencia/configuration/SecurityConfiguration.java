package mx.uaq.correspondencia.configuration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import mx.uaq.correspondencia.security.UaqAuthenticationProvider;
import mx.uaq.correspondencia.security.UaqUserDetailsService;
import mx.uaq.correspondencia.security.UaqWebAuthenticationFailureHandler;
import mx.uaq.correspondencia.security.UaqWebUrlAuthenticationSuccessHandler;



@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	public static final String LOGOUT_PERMISO_DENEGADO = "/api/logout?causalLogout=permisoDenegado";
	
	@Autowired private UaqWebAuthenticationFailureHandler uaqWebAuthenticationFailureHandler;
	@Autowired private UaqWebUrlAuthenticationSuccessHandler uaqWebUrlAuthenticationSuccessHandler;
	
	@Bean
	@Override
	public AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(getAuthenticationProvider());
	}
	
	@Override
	public void configure(WebSecurity wb) throws Exception {
		wb.ignoring()
			.antMatchers("/sources/**/*.{js, html, css, png, woff}");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.exceptionHandling().accessDeniedHandler(createDeniedHandler())
			.and()
			.formLogin()
			.loginProcessingUrl("/api/authenticate")
			.loginPage("/login")
			.failureHandler(uaqWebAuthenticationFailureHandler)
			.successHandler(uaqWebUrlAuthenticationSuccessHandler)
			.usernameParameter("username")
			.passwordParameter("password")
			.permitAll()
			.and()
			.logout()
			.logoutUrl("/api/logout")
			.invalidateHttpSession(true)
			.clearAuthentication(true)
			.logoutSuccessHandler(new LogoutSuccessHandler() {
				
				@Override
				public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
						throws IOException, ServletException {
					Object causaLogout = request.getParameter("causaLogout");
					String causaLogoutQuery = (causaLogout == null) ? "logout" : causaLogout.toString();
					response.setStatus(HttpStatus.OK.value());
					response.sendRedirect(String.format("%s/%s?%s", request.getContextPath(), "", causaLogoutQuery));
					response.getWriter().flush();
					
				}
			})
			.deleteCookies("JSESSIONID")
			.permitAll()
			.and()
			.headers()
			.frameOptions()
			.sameOrigin()
			.and()
			.authorizeRequests()
			.antMatchers("/administrador/**").hasRole("ADMINISTRADOR")
			.and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
			.maximumSessions(1)
			.sessionRegistry(sessionRegistry())
			.maxSessionsPreventsLogin(false)
			.expiredUrl("/login?expiredSession")
			.and()
			.invalidSessionUrl("/login?expiredSession")
			.sessionFixation()
			.newSession()
			.and()
			.csrf()
			.disable();
	}
	
	@Bean
	public AccessDeniedHandler createDeniedHandler() {
		return new AccessDeniedHandler() {

			@Override
			public void handle(HttpServletRequest request, HttpServletResponse response,
					AccessDeniedException accessDeniedException) throws IOException, ServletException {
				response.sendRedirect(request.getContextPath() + LOGOUT_PERMISO_DENEGADO);
			}	
		};
	}
	
	@Bean
	public AuthenticationProvider getAuthenticationProvider() {
		return new UaqAuthenticationProvider();
	}
	
	@Bean
	public UserDetailsService userDetailsService() {
		return new UaqUserDetailsService();
	}
	
	@Bean
	public HttpSessionEventPublisher httpSessionEventPublisher() {
		return new HttpSessionEventPublisher();
	}
	
	@Bean
	public SessionRegistry sessionRegistry() {
		return new SessionRegistryImpl();
	}
}