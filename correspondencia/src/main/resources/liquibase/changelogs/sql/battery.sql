-- ====================================== USUARIOS ======================================
INSERT INTO USUARIO (ID, CLAVE, NOMBRE, AP_PATERNO, AP_MATERNO, NOMBRE_COMPLETO, TIPO, ESTATUS, FALLOS, CORREO, CORREO_INSTITUCIONAL, ESTATUS_NIP, TELEFONO, FECHA_ALTA)
VALUES (SEQ_USUARIO.NEXTVAL, '15586', 'Christian', 'Cervantes', 'Olguin', 'Christian Cervantes Olguin', 'TRABAJADOR_UAQ', 'ACTIVO', 0, 'chrcervantes.389@gmail.com', 'christian.cervantes@uaq.mx', 'NO_APLICA', '442 3453937', SYSDATE);
INSERT INTO ROL_USUARIO (ID_ROL, ID_USUARIO) VALUES (1, 1);

COMMIT;